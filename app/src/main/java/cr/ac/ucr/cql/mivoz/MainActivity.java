package cr.ac.ucr.cql.mivoz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    // Lenguaje
    private String mLenguaje = "en-US";
    private Locale mLocale = Locale.US;
    private String souceText;
    // Texto y botones iniciales de la actividad principal
    TextView mTexto;
    // Opcion de Voz a texto
    Button mSpeechText;
    // Lista de textos encontrados
    ArrayList<String> mMatchesText;
    // Lista para mostrar los textos
    ListView mTextListView;
    // Dialogo para mostrar la lista
    Dialog mMatchTextDialog;
    // Instancia de Texto a voz
    TextToSpeech mTextToSpeech;
    // Texto de entrada
    private EditText mEditText;
    // Opcion de Texto a voz
    Button mTextSpeech;
    //Opcion de traducir
    Button mTranslate;
    // Intancia de texto traducido
    TextView mTraduced;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // componentes de la aplicacion
        mSpeechText = (Button) findViewById(R.id.buttonSpeechText);
        mTextSpeech = (Button) findViewById(R.id.buttonTextSpeech);
        mTexto = (TextView) findViewById(R.id.texto);
        mEditText = (EditText) findViewById(R.id.editText);
        mTranslate = (Button) findViewById(R.id.translate);
        mTraduced = (TextView) findViewById(R.id.traduced);


        // intent para el reconocimiento de voz

        mSpeechText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    // intent al API de reconocimiento de voz
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, mLenguaje);
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "No hay conexión a Internet",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Set el lenguaje de texto a voz
        mTextToSpeech = new TextToSpeech(getApplicationContext(), new
                TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        mTextToSpeech.setLanguage(mLocale);
                    }
                });
        // Intente para texto a voz
        mTextSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // lenguaje
                mTextToSpeech.setLanguage(mLocale);
                // Texto el edit
                String toSpeak = mEditText.getText().toString();
                Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // Mayor que Lollipop
                    mTextToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            }
        });

        /*Traducir el texto*/
        mTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                souceText = mEditText.getText().toString();
                //Instanciamos según lenguaje el idioma
                //Si está el check en Inglés se traduce el texto a inglés
                if ( mLenguaje == "en-US"){
                    //Instancia de Firebase ES-EN
                    FirebaseTranslatorOptions options =
                            new FirebaseTranslatorOptions.Builder()
                                    .setSourceLanguage(FirebaseTranslateLanguage.ES)
                                    .setTargetLanguage(FirebaseTranslateLanguage.EN)
                                    .build();
                    final FirebaseTranslator englishToSpanish =
                            FirebaseNaturalLanguage.getInstance().getTranslator(options);

                    //Se descarga el modelo requerido
                    FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder()
                            .requireWifi()
                            .build();
                    englishToSpanish.downloadModelIfNeeded(conditions)
                            .addOnSuccessListener(
                                    new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void v) {
                                            //Se comienza la traducción
                                            englishToSpanish.translate(souceText)
                                                    .addOnSuccessListener(new OnSuccessListener<String>() {
                                                @Override
                                                public void onSuccess(String s) {
                                                    mTraduced.setText(s);
                                                }
                                            });
                                        }
                                    });
                } else if(mLenguaje == "es-ES") {
                    //Instancia de Firebase EN-ES
                    // Si esta el check en español se traduce a español
                    FirebaseTranslatorOptions options =
                            new FirebaseTranslatorOptions.Builder()
                                    .setSourceLanguage(FirebaseTranslateLanguage.EN)
                                    .setTargetLanguage(FirebaseTranslateLanguage.ES)
                                    .build();
                    final FirebaseTranslator spanishToEnglish =
                            FirebaseNaturalLanguage.getInstance().getTranslator(options);
                    //Se descarga el modelo requerido
                    FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder()
                            .requireWifi()
                            .build();
                    spanishToEnglish.downloadModelIfNeeded(conditions)
                            .addOnSuccessListener(
                                    new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void v) {
                                            //Se comienza la traducción
                                            spanishToEnglish.translate(souceText)
                                                    .addOnSuccessListener(new OnSuccessListener<String>() {
                                                        @Override
                                                        public void onSuccess(String s) {
                                                            mTraduced.setText(s);
                                                        }
                                                    });
                                        }
                                    });
                }
            }
        });
    }

    // evento se seleccion de idioma
    public void onRadioButtonClicked(View view) {
        // Verificar el RadioButton seleccionado
        boolean checked = ((RadioButton) view).isChecked();
        // Determinar cual RadioButton esta seleccionado
        switch (view.getId()) {
            case R.id.radioButtonIngles:
                if (checked)
                    mLenguaje = "en-US";
                mLocale = Locale.US;
                break;
            case R.id.radioButtonEspannol:
                if (checked)
                    mLenguaje = "es-ES";
                mLocale = new Locale("spa", "ESP");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Valores de retorn del intent
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            // Si retorna resultados el servicion de reconocimiento de voz
            // Creamos el dialogo y asignamos la lista
            mMatchTextDialog = new Dialog(MainActivity.this);
            mMatchTextDialog.setContentView(R.layout.dialog_matches_frag);
            // título del dialogo
            mMatchTextDialog.setTitle("Seleccione el texto");
            // Lista de elementos
            mTextListView = (ListView) mMatchTextDialog.findViewById(R.id.listView1);
            mMatchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            // Mostramos los datos en la lista
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, mMatchesText);
            mTextListView.setAdapter(adapter);
            // Asignamos el evento del clic en la lista
            mTextListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long
                        id) {
                    mTexto.setText("You have said: " + mMatchesText.get(position));
                    mEditText.setText(mMatchesText.get(position));
                    mMatchTextDialog.hide();
                }
            });
            mMatchTextDialog.show();
        }
    }

    // verificar conexion a internet
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
